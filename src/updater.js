function debug() {
    try {
        return (enable_debug());
    } catch (_e) {
        return false;
    }
}

function get_value(id) {
    return document.getElementById(id).value;
}


function set_HTML(id, ctt) {
    document.getElementById(id).innerHTML = ctt;
}


function set_value(id, ctt) {
    document.getElementById(id).value = ctt;
}


function refresh_keys(keys) {
    if (keys !== null) {
        if (keys.pkh === null) {
            set_HTML('public-key-hash', 'unknown');
        } else {
            set_HTML('public-key-hash', keys.pkh);
        }
        if (keys.pk === null) {
            set_HTML('public-key', 'unknown');
        } else {
            set_HTML('public-key', keys.pk);
        }
        if (keys.pkh === null) {
            set_HTML('dunscan', 'unknown');
        } else {
            let url = data.mk_explorer_link(keys.pkh);
            ctt = '<a target=_blank href="' + url + '">' + url + '</a>';
            set_HTML('dunscan', ctt);
        }
    }
}


function refresh_balance(balance) {
    if (balance === null) {
        set_HTML('balance', 'unknown');
    } else {
        set_HTML('balance', (balance / 1000000) + " DUN");
    }
}


function refresh_revealed(revealed) {
    if (revealed === null) {
        set_HTML('revealed', 'unknown');
    } else {
        if (revealed) {
            set_HTML('revealed', "Yes");
        } else {
            set_HTML('revealed', "Not yet");
        }
    }
}


function do_update_revealed() {
    return (new Promise(function (resolve, reject) {
        data.update_revealed().then(function (rev) {
            refresh_revealed(rev);
        });
        resolve(1);
    }));
}


function do_update_balance() {
    return (new Promise(function (resolve, reject) {
        data.update_balance().then(function (bal) {
            refresh_balance(bal);
        });
        resolve(1);
    }));
}

function do_update_derivation_path() {
    let ui_derivation_path = get_value("derivation-path");
    let data_derivation_path = data.get_derivation_path();
    if (ui_derivation_path !== data_derivation_path) {
        new Promise(function (resolve, _reject) {
            refresh_keys({pkh: null, pk: null});
            refresh_balance(null);
            refresh_revealed(null);
            resolve(true);
        }).then(function (_x) {
            data.update_derivation_path(ui_derivation_path).then(function (keys) {
                refresh_keys(keys);
                do_update_revealed().then(function (_r) {
                    do_update_balance().then(function (_r) {
                        return true;
                    });
                });
            });
        });
    }
}


function network_changed() {
    let network = get_value("network");
    data.update_network(network).then(function (_result) {
        do_update_revealed().then(function (_r) {
            do_update_balance().then(function (_r) {
                refresh_keys(data.get_keys()); // to update dunscan link
                return true;
            });
        });
    });
}

function _show_message(id, popup_kind, message) {
    var msg = '';
    msg += '<div class=\"text-center alert ' + popup_kind + ' alert-dismissible fade show col-md-12 row" role="alert">';
    msg += message;
    msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
    msg += '<span aria-hidden="true" onclick="set_HTML(\'' + id + '\',\'\')">&times;</span>';
    msg += '</button>';
    msg += '</div>';
    set_HTML(id, msg);
}


function do_send() {
    do_update_revealed().then(function (_r) {
        do_update_balance().then(function (_r) {
            let destination = get_value("destination");
            let amount = parseFloat(get_value("amount"));
            let fees = parseFloat(get_value("fees"));
            let gas_limit = parseInt(get_value("gas_limit"));
            let storage_limit = parseInt(get_value("storage_limit"));

            data.make_transfer(destination, amount, fees, gas_limit, storage_limit).then(
                    function (hash) {
                        let url = data.mk_explorer_link(hash);
                        var msg = '';
                        msg += 'Operation injected: <a target=_blank href="' + url + '">' + hash + '</a>';
                        _show_message('transfer-status', 'alert-info', msg);
                    }
            ).catch(function (err) {
                _show_message('transfer-status', 'alert-danger', err);
            });
        });
    });
}

function do_infer_parameters() {
    do_update_revealed().then(function (_r) {
        do_update_balance().then(function (_r) {
            let destination = get_value("destination");
            let amount = get_value("amount");
            data.infer_parameters(destination, amount).then(
                    function (params) {
                        set_value('fees', (params.immediate_fees + params.reveal_fees) / 1000);
                        set_value('burn', params.burn / 1000);
                        set_value('gas_limit', params.gas_limit);
                        set_value('storage_limit', params.storage_limit);
                        if (amount === 'Empty source') {
                            set_value('amount', (params.amount / 1000000));
                        }
                        var msg = '';
                        msg += 'Parameters inferred';
                        _show_message('transfer-status', 'alert-info', msg);
                    }
            ).catch(function (err) {
                _show_message('transfer-status', 'alert-danger', err);
            });
        });
    });
}


function init_page() {
    data.init_trezor();
    let saved = data.restore_fields();
    if (saved !== null) {
        if (saved.network !== null) {
            set_value('network', saved.network);
        }
        if (saved.derivation_path !== null) {
            set_value('derivation-path', saved.derivation_path);
        }
        data.update_network(get_value("network")).then(function (status) {
            do_update_derivation_path();
        });
    }
}

var intervalID = setInterval(function () {
    do_update_revealed().then(function (_r) {
        do_update_balance().then(function (_r) {
            return true;
        });
    });
}, 5000);
