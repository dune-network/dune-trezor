var rpc = {}; // namespace

function debug() {
    try {
        return (enable_debug());
    } catch (_e) {
        return false;
    }
}

rpc.mk_rpc = function (node_addr, req, data) {
    return new Promise(function (resolve, reject) {
        var path = node_addr + req;
        let xhr = new XMLHttpRequest();
        let kind = (data === null || data === undefined) ? 'GET' : 'POST';
        xhr.open(kind, path, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        if (debug())
            console.log('sending ' + kind + ' rpc ' + path + ' with data ' + data);
        xhr.send(data);
        xhr.onload = function () {
            if (debug()) {
                console.log('succeeded xhr status = ' + xhr.status);
                console.log('succeeded xhr response = ' + xhr.response);
            }
            resolve({
                success: (xhr.status >= 200 && xhr.status < 300),
                status: xhr.status,
                response: JSON.parse(xhr.response)
            });
        };
        xhr.onerror = function () {
            if (debug()) {
                console.log('failed xhr status = ' + xhr.status);
                console.log('failed xhr response = ' + xhr.response);
            }
            {
                try {
                    response = JSON.parse(xhr.response);
                } catch (_ex) {
                    response = {};
                }
            }
            reject({
                success: false,
                status: xhr.status,
                response: response
            });
        };
    }
    );
};


rpc.manager_key = function (pkh, node_addr) {
    return rpc.mk_rpc(
            node_addr,
            "/chains/main/blocks/head/context/contracts/" + pkh + "/manager_key");
};


// revealed: compatible with 004 and 005
rpc.revealed = function (pkh, node_addr) {
    return new Promise(function (resolve, reject) {
        rpc.mk_rpc(
                node_addr,
                "/chains/main/blocks/head/context/contracts/" + pkh + "/manager_key").then(function (res) {
            if (!res.success) {
                resolve({success: false, revealed: null});
            } else {
                if (res.response === null) {
                    resolve({success: true, revealed: false});
                } else {
                    if (res.response.manager === undefined) { // Protocol >= 005
                        resolve({success: true, revealed: true});
                    } else { // protocol <= 004
                        resolve({success: true, revealed: res.response.key !== undefined});
                    }
                }
            }
        });
    });
};

rpc.balance = function (pkh, node_addr) {
    return rpc.mk_rpc(
            node_addr,
            "/chains/main/blocks/head/context/contracts/" + pkh + "/balance");
};


rpc.headHash = function (node_addr) {
    return rpc.mk_rpc(
            node_addr,
            "/chains/main/blocks/head/hash"
            );
};


rpc.counter = function (pkh, node_addr) {
    return rpc.mk_rpc(
            node_addr,
            "/chains/main/blocks/head/context/contracts/" + pkh + "/counter"
            );
};


rpc.injection = function (data, node_addr) {
    return rpc.mk_rpc(
            node_addr,
            "/injection/operation?chain=main",
            data);
};
