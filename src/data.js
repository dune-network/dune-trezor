let data = {
    node: null,
    explorer: null,
    pkh: null,
    pk: null,
    dn1: null,
    balance: null,
    revealed: null,
    contracts: null,
    derivation_path: null,
    TrezorConnect: window.TrezorConnect,
    trezor_connect_version: 8, // version that works with protocol 005
    default_fees: 1285, // in muDUN = 1.285 mDUN
    default_burn: 257000, // muDUN = 257 mDUN = 0.257 DUN
    default_gas_limit: 10300,
    default_storage_limit: 300,
    delta_gas_empty_account: 740,
    delta_fees_empty_account: 74
};


function debug() {
    try {
        return (enable_debug());
    } catch (_e) {
        return false;
    }
}
;


data.mk_explorer_link = function (path) {
    return (data.explorer + '/' + path);
};

data.get_derivation_path = function () {
    return data.derivation_path;
};

data.get_keys = function () {
    return {pk: data.pk, pkh: data.pkh, dn1: data.dn1};
};

data.init_trezor = function () {
    if (data.trezor_connect_version >= 7) {
        data.TrezorConnect.manifest({
            email: 'contact@dune.network',
            appUrl: 'https://dune.network/dune-trezor'
        });
        if (debug())
            console.log("Trezor inited. Version is: " + data.trezor_connect_version);
    }
};


data.update_network = function (network) {
    return new Promise(function (resolve, reject) {
        if (network === undefined || network === null || network === "") {
            reject(false)
        } else {
            let node = 'https://' + network + '-node.dunscan.io';
            let exp_prefix = (network === "mainnet") ? '' : network + '.';
            let explorer = 'https://' + exp_prefix + 'dunscan.io';
            data.node = node;
            data.explorer = explorer;
            localStorage.setItem('network', network);
            resolve(true);
        }
    });
};


data.update_revealed = function () {
    return new Promise(function (resolve, reject) {
        if (data.pkh !== null)
            if (data.node === null) {
                reject("update_revealed: field node is null");
            } else {
                rpc.revealed(data.pkh, data.node).then(function (res) {
                    if (!res.success) {
                        reject("update_revealed: RPC failed: " + JSON.stringify(res));
                    } else {
                        data.revealed = res.revealed;
                        resolve(data.revealed);
                    }
                });
            }
    });
};


data.update_balance = function () {
    return new Promise(function (resolve, reject) {
        if (data.pkh !== null)
            if (data.node === null) {
                reject("update_balance: field node is null");
            } else {
                rpc.balance(data.pkh, data.node).then(function (res) {
                    if (!res.success) {
                        reject("update_balance: RPC failed: " + JSON.stringify(res.response));
                    } else {
                        data.balance = parseInt(res.response);
                        resolve(data.balance);
                    }
                });
            }
    });
};


data.update_derivation_path = function (new_path) {
    return new Promise(function (resolve, reject) {
        data.pk = null;
        data.pkh = null;
        data.dn1 = null;
        if (new_path !== null && new_path !== undefined && new_path !== "") {
            if (data.derivation_path === new_path) {
                resolve({pk: data.pk, pkh: data.pkh, dn1: data.dn1});
            } else {
                data.derivation_path = new_path;
                savedPkh = localStorage.getItem('pkh:' + new_path);
                savedDn1 = localStorage.getItem('dn1:' + new_path);
                savedPk = localStorage.getItem('pk:' + new_path);

                if (savedPkh !== null && savedPk !== null /*&& savedDn1 !== null*/) {
                    // all data are found in local storage
                    data.pkh = savedPkh;
                    data.pk = savedPk;
                    data.dn1 = savedDn1;
                    localStorage.setItem('derivation-path', new_path); // save current derivation path
                    resolve({pk: data.pk, pkh: data.pkh, dn1: data.dn1});
                } else {
                    // Get Tezos Address
                    data.TrezorConnect.tezosGetAddress({
                        path: data.derivation_path
                    }).then(function (r) {
                        let res = JSON.stringify(r);
                        if (r.success) {
                            let pkh = r.payload.address;
                            if (debug())
                                console.log(res);
                            // Get Tezos Public Key
                            data.TrezorConnect.tezosGetPublicKey({
                                path: data.derivation_path
                            }).then(function (r) {
                                let res = JSON.stringify(r);
                                if (r.success) {
                                    let pk = r.payload.publicKey;
                                    if (debug())
                                        console.log(res);
                                    /*rpc.manager_key(pkh, data.node).then(function (res) {
                                     if (!res.success) {
                                     reject("update_derivation_path: RPC failed: " + JSON.stringify(res.response));
                                     } else {
                                     let dn1 = res.response.manager;
                                     */
                                    let dn1 = null;
                                    // update state
                                    data.dn1 = dn1;
                                    data.pkh = pkh;
                                    data.pk = pk;
                                    // save data in local storage for future use
                                    localStorage.setItem('dn1:' + new_path, data.dn1);
                                    localStorage.setItem('pkh:' + new_path, data.pkh);
                                    localStorage.setItem('pk:' + new_path, data.pk);
                                    localStorage.setItem('derivation-path', new_path);
                                    resolve({pk: data.pk, pkh: data.pkh, dn1: data.dn1});
                                    /*}
                                     }, function (err) {
                                     reject("update_derivation_path: not able to get pk: " +
                                     JSON.stringify(err));
                                     });*/

                                } else {
                                    reject("update_derivation_path: not able to get pkh: " + res);
                                }
                            }, function (err) {
                                reject("update_derivation_path: not able to get pkh: " +
                                        JSON.stringify(err));
                            });
                        }
                    });
                }
            }
        }
    });
};


data.parse_amount = function (amount_input, cost) {
    return new Promise(function (resolve, reject) {
        if (amount_input === 'Empty source') {
            let amount = data.balance - cost;
            if (amount < 0) {
                reject('parse_amount: balance too low');
            } else {
                resolve(amount);
            }
        } else {
            let amnt = parseFloat(amount_input);
            if (isNaN(amnt)) {
                reject("parse_amount: invalid amount");
            } else if (amount < 0) {
                reject('parse_amount: balance too low');
            } else {
                resolve(amnt * 1000000.); // to muDUN
            }
        }
    });
};


data.infer_parameters = function (destination, amount_input) {
    return new Promise(function (resolve, reject) {
        if (data.node === null) {
            reject("infer_parameters: field node is null");
        } else if (data.pkh === null) {
            reject("infer_parameters: field public key hash is null");
        } else if (data.balance === null) {
            reject("infer_parameters: field balance is null");
        } else if (data.derivation_path === null) {
            reject("infer_parameters: field derivation path is null");
        } else {
            rpc.balance(destination, data.node).then(function (res) {
                if (!res.success)
                    reject("infer_parameters: destination seems to be invalid.");
                else {
                    let empty_source = (amount_input === 'Empty source');
                    let dest_is_fresh = parseInt(res.response) === 0;
                    let will_allocate_src = (empty_source && destination === data.pkh);
                    let immediate_fees = data.default_fees;
                    let reveal_fees = (data.revealed) ? 0 : data.default_fees;
                    let burn = (dest_is_fresh) ? data.default_burn : 0;
                    let gas_limit = data.default_gas_limit;
                    let storage_limit = (dest_is_fresh || will_allocate_src) ? data.default_storage_limit : 0;
                    let cost = immediate_fees + reveal_fees + burn;
                    data.parse_amount(amount_input, cost).then(function (amount) {
                        // detect if account will be emptyed
                        if (amount + cost === data.balance) {
                            immediate_fees += data.delta_fees_empty_account;
                            gas_limit += data.delta_gas_empty_account;
                            if (empty_source) {
                                amount -= data.delta_fees_empty_account;
                            }
                        }
                        if (amount + cost > data.balance) {
                            reject("infer_parameters: balance too low.");
                        } else {
                            resolve({
                                immediate_fees: immediate_fees,
                                reveal_fees: reveal_fees,
                                burn: burn,
                                gas_limit: gas_limit,
                                storage_limit: storage_limit,
                                amount: amount
                            });
                        }
                    }, function (err) {
                        reject(err);
                    });
                }
            }
            , function (_error) {
                reject("infer_parameters: destination seems to be invalid.");
            });
        }
    });
};


data.sign_and_inject_operation = function (operation) {
    return new Promise(function (resolve, reject) {
        data.TrezorConnect.tezosSignTransaction(operation).then(function (res) {
            if (!res.success) {
                reject("sign_and_inject_operation: error while signing the operation: " + JSON.stringify(res));
            } else {
                if (data.node === null) {
                    reject("sign_and_inject_operation: field node is null");
                } else {
                    let signed_op = JSON.stringify(res.payload.sig_op_contents);
                    rpc.injection(signed_op, data.node).then(function (res) {
                        if (res.status !== 200) {
                            reject('sign_and_inject_operation failed: ' + JSON.stringify(res.response));
                        } else {
                            resolve(res.response); // op hash
                        }
                    });
                }
            }
        }, function (error) {
            reject("sign_and_inject_operation: error while signing the operation: " + JSON.stringify(error));
        });
    });
};


data.make_transfer = function (destination, amount, fees, gas_limit, storage_limit) {
    return new Promise(function (resolve, reject) {
        if (isNaN(fees)) {
            reject("make_transfer: invalid fees");
        } else if (isNaN(gas_limit)) {
            reject("make_transfer: invalid gas limit");
        } else if (isNaN(storage_limit)) {
            reject("make_transfer: invalid storage limit");
        } else if (data.pk === null) {
            reject("make_transfer: field public key is null");
        } else if (data.balance === null) {
            reject("make_transfer: field balance is null");
        } else if (data.derivation_path === null) {
            reject("make_transfer: field derivation path is null");
        } else {
            data.infer_parameters(destination, amount).then(function (inferred_params) {
                // amount checked by infer_parameters and transtaled to muDUN
                let amount = inferred_params.amount;
                let mu_fees = fees * 1000.; // external fees in muDUN
                if (mu_fees < inferred_params.immediate_fees + inferred_params.reveal_fees) {
                    reject("make_transfer: fees too low.");
                } else if (gas_limit < inferred_params.gas_limit) {
                    reject("make_transfer: gas_limit too low");
                } else if (storage_limit < inferred_params.storage_limit) {
                    reject("make_transfer: storage_limit too low");
                } else {
                    rpc.headHash(data.node).then(function (res) {
                        if (!res.success)
                            reject("make_transfer: failed to get the current head of the chain.");
                        else {
                            let headHash = res.response;
                            rpc.counter(data.pkh, data.node).then(function (res) {
                                if (!res.success)
                                    reject("make_transfer: failed to get the current counter of your address");
                                else {
                                    let counter = parseInt(res.response);
                                    let ops = {};
                                    if (!data.revealed) {
                                        counter = counter + 1;
                                        ops.reveal = {
                                            source: data.pkh,
                                            counter: counter,
                                            fee: inferred_params.reveal_fees,
                                            gas_limit: inferred_params.gas_limit,
                                            storage_limit: 0,
                                            public_key: data.pk
                                        };
                                    }
                                    counter = counter + 1;
                                    ops.transaction = {
                                        source: data.pkh,
                                        destination: destination,
                                        counter: counter,
                                        amount: amount,
                                        fee: mu_fees - inferred_params.reveal_fees,
                                        gas_limit: gas_limit,
                                        storage_limit: storage_limit
                                    };
                                    let op = {
                                        path: data.derivation_path,
                                        branch: headHash,
                                        operation: ops
                                    }
                                    if (debug())
                                        console.log(JSON.stringify(op));
                                    data.sign_and_inject_operation(op).then(function (res) {
                                        resolve(res);
                                    }, function (err) {
                                        reject(err);
                                    });
                                }
                            });
                        }
                    });
                }
            }
            , function (error) {
                reject(error);
            }
            );
        }
    });
};

data.restore_fields = function () {
    return ({
        network: localStorage.getItem('network'),
        derivation_path: localStorage.getItem('derivation-path')
    });
};
