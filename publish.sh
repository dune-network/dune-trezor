echo "usage"
echo "   ./publish default"
echo "   ./publish next <remote-subdir>"
echo ""

case $1 in
    default)
        rsync -aSvuc src/index.html src/data.js src/rpc.js src/updater.js src/images dune.network:/home/www.dune.network/www/dune-trezor/
	;;
    next)
        remote_subdir=$2
        if [ "$remote_subdir" = "" ] ; then
            echo "No remote sub-dir provided"
            exit 1
        fi
        rsync -aSvuc src/index.html src/data.js src/rpc.js src/updater.js src/images dune.network:/home/www.dune.network/www/dune-trezor/$remote_subdir
	;;
    "")
	echo "argument required"
	;;
    *)
	echo "bad argument $1"
	;;
esac
