# Support for Trezor model T in Dune Network

Small Web wallet to transfer $DUN coins from an address managed by a Trezor
model T hardware device to another address.

The content of this repository (src directory) is available online at this
address: [https://dune.network/dune-trezor/](https://dune.network/dune-trezor/).
